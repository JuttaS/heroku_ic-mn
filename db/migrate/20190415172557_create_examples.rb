class CreateExamples < ActiveRecord::Migration[5.1]
  def change
    create_table :examples do |t|
      t.string :title
      t.text :body
      t.references :portfolio, foreign_key: true
      t.json :images
      t.string :audio_link
      t.string :video_link

      t.timestamps
    end
  end
end
