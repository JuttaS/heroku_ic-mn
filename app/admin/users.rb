ActiveAdmin.register User do
	permit_params :roles, :name, :email
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

index do
  column :name
  column :email
  column :roles
  actions
end

 filter :name
 filter :email
 filter :roles

 form do |f|
    f.inputs do
      f.input :roles, as: :radio, collection: ['site_admin', 'editor', 'user']
    end
    f.actions
  end



end
