class ChangeColumnAudioLink < ActiveRecord::Migration[5.1]
  def change
  	rename_column :examples, :audio_link, :audio
  end
end
