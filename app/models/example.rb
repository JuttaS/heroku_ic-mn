class Example < ApplicationRecord
  belongs_to :portfolio
  validates_presence_of :title, :body

   mount_uploader :audio, AudioUploader
   mount_uploaders :images, ImagesUploader
end