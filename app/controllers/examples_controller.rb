class ExamplesController < ApplicationController
	before_action :set_portfolio_item, only: [:edit, :new, :create, :destroy]
	before_action :set_example_item, only: [:edit, :update, :destroy]
    layout 'portfolio'
    access site_admin: :all, editor: :all
  

  def new
  	@example_item = Example.new
  end


  def create
  	@example_item = @portfolio_item.examples.build(example_params)

  	if @example_item.save
      redirect_to portfolio_show_path(@portfolio_item), notice: 'Dein Beitrag wurde gespeichert.' 
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @example_item.update(example_params)
      redirect_to portfolio_show_path(@example_item.portfolio), notice: 'Dein Beitrag wurde aktualisiert'
    else
      render :edit
    end
  end

  def destroy 
    @example_item.destroy

    redirect_to portfolio_show_path(@portfolio_item), notice: 'Dein Beitrag wurde gelöscht'
  end

  private

  def example_params
    params.require(:example).permit(:title,
                                      :body,
                                      :images,
                                      :audio,
                                      :video_link,
                                      {images: []}
                                     )
  end



  def set_example_item
  	@example_item = Example.find(params[:id])
  
  end

  def set_portfolio_item
    @portfolio_item = Portfolio.find(params[:portfolio_id])
  end
end