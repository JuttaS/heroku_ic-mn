class PagesController < ApplicationController
  def home
    @posts = Blog.all
    @skills = Skill.all
     @page_title = "IC-MN"
  end

  def about
    @skills = Skill.all
     @page_title = "IC-MN"
  end

  def contact
    @page_title = "IC-MN Kontakt"

  end

  def tech_news
    @tweets = SocialTool.twitter_search
  end
end
